package rob.pyr.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import rob.pyr.rest.feign.FeignServiceFactory;
import rob.pyr.rest.feign.MicroServiceBClient;

@Configuration
@RequiredArgsConstructor
public class ServiceAConfiguration {

    private final FeignServiceFactory feignServiceFactory;

    @Bean
    public MicroServiceBClient microServiceBClient() {
        return feignServiceFactory.CreateMicroServiceBClient();
    }
}
