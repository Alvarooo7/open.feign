package rob.pyr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import rob.pyr.service.ServiceAConfiguration;

@SpringBootApplication
@ComponentScan(basePackages = {"rob.pyr.controller", "rob.pyr.rest.feign"})
@Import({ServiceAConfiguration.class})
public class App {

    public static void main(String[] args) {

        SpringApplication.run(App.class, args);

    }
}
