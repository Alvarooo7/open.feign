package rob.pyr.rest.feign;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class FeignServiceFactory {

    private final String serviceBURL;

    public FeignServiceFactory(@Value("${feign.serviceb.url}") String url) {
        this.serviceBURL = url;
    }

    //ServiceName is service with proper url to serviceB
    /*public ServiceName createServiceName(String username, String password) {
        BasicAuthorizationInterceptor basicAuthorizationInterceptor = new BasicAuthorizationInterceptor(username, password);

        return getFeignBuilder(basicAuthorizationInterceptor).target(ServiceName.class, serviceBURL);
    }*/

    public MicroServiceBClient CreateMicroServiceBClient() {
        return getFeignBuilder().target(MicroServiceBClient.class, serviceBURL);
    }

    //BasicAuthorizationInterceptor basicAuthorizationInterceptor as param in method
    private Feign.Builder getFeignBuilder() {
        ObjectMapper mapper = jacksonDataFormat();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        return Feign.builder()
                .decoder(new JacksonDecoder(mapper))
                .encoder(new JacksonEncoder(mapper));
                //.requestInterceptor(basicAuthorizationInterceptor)
    }

    private ObjectMapper jacksonDataFormat() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.registerModule(javaTimeModule());

        return objectMapper;
    }

    private JavaTimeModule javaTimeModule() {
        JavaTimeModule module = new JavaTimeModule();
        module.addDeserializer(LocalDate.class, new LocalDateDeserializer(DateTimeFormatter.ISO_DATE));
        module.addSerializer(new LocalDateSerializer(DateTimeFormatter.ISO_DATE));

        return module;
    }

}
