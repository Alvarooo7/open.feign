package rob.pyr.endpoints;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rob.pyr.dto.Dummy;

@RestController
@RequestMapping("microserviceB")
public class dummyEndpointBService {

    @RequestMapping(method = RequestMethod.GET, value = "getMessage", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Dummy getMessage() {
        return new Dummy("hello from B service!");
    }
}
